class BoisController < ApplicationController
  before_action :set_boi, only: %i[ show edit update destroy ]

  # GET /bois or /bois.json
  def index
    @bois = Boi.all
  end

  # GET /bois/1 or /bois/1.json
  def show
  end

  # GET /bois/new
  def new
    @boi = Boi.new
  end

  # GET /bois/1/edit
  def edit
  end

  # POST /bois or /bois.json
  def create
    @boi = Boi.new(boi_params)

    respond_to do |format|
      if @boi.save
        format.html { redirect_to @boi, notice: "Boi was successfully created." }
        format.json { render :show, status: :created, location: @boi }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @boi.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /bois/1 or /bois/1.json
  def update
    respond_to do |format|
      if @boi.update(boi_params)
        format.html { redirect_to @boi, notice: "Boi was successfully updated." }
        format.json { render :show, status: :ok, location: @boi }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @boi.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /bois/1 or /bois/1.json
  def destroy
    @boi.destroy
    respond_to do |format|
      format.html { redirect_to bois_url, notice: "Boi was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_boi
      @boi = Boi.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def boi_params
      params.require(:boi).permit(:name, :author, :subj, :price)
    end
end
