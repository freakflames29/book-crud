class FoodsController < ApplicationController
  def index
  	@foods=Food.all
  end

  def show
  	@food=Food.find(params[:id])
  end

  def new
  	@food=Food.new

  end
  def create
  	@food=Food.new(food_para)
  	if @food.save
  		redirect_to foods_path
  		flash[:notice]=@food.name+" succesfully created"
  	else
  		render 'new'
  	end
  end

  def show
  	@food=Food.find(params[:id])

  end

  def edit
  	@food=Food.find(params[:id])


  end

  def destroy
  	@food=Food.find(params[:id])

  	if @food.destroy
  		redirect_to foods_path
  		flash[:notice]=@food.name+" succesfully deleted"

  	end


  end
  def update
  	@food=Food.find(params[:id])
  	if @food.update(food_para)
  		redirect_to foods_path
  		flash[:notice]=@food.name+" succesfully updated"
  	else
  		redner 'show'
  	end



  end
  private
  def food_para
  	params.require(:food).permit(:name,:rating,:price)

  end

end
