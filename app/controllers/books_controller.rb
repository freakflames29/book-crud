class BooksController < ApplicationController
	def index
		@books=Book.all
	end
	def new
		@book=Book.new
	end
	def create
		@book=Book.new(book_param)
		if @book.save
			flash[:notice]="Success" #showing flash mesage throw flash method
			
			redirect_to @book # @books redirects user to show page
		else
			render 'new' # it renders the new.html.erb template
		end

		
	end
	def edit
		@book=Book.find(params[:id])

	end
	def update
		@book=Book.find(params[:id])
		@book.update(book_param)
		redirect_to books_path

	end
	def book_param
		params.require(:book).permit(:name,:subj,:author,:price)
	end
	def show
		@book=Book.find(params[:id])

	end
	def destroy
		@book=Book.find(params[:id])
		if @book.destroy
			redirect_to books_path
		end
	end
end
