class RegistrationsController < ApplicationController
	def new
		@user=User.new
	end
	def create
		@user=User.new(reg_param)
		if @user.save
			session[:user_id]=@user.id # storing the user id in session storage
			redirect_to root_path, notice:"Signed Up successfully"
		else

			render :new

		end


		# render plain:"Sourav das" # it used to render plain text
	end

	private 
	def reg_param
		params.require(:user).permit(:email,:password,:password_confirmation)
	end


end