class MainsController < ApplicationController
	def index
		if session[:user_id]
			@user=User.find_by(id:session[:user_id])
			# if we use User.find and destroy the user in our db it will throw an error bcz it is still finding the destoryed id 

			#but if we use find_by method it will not throw an error bcoz if the user get destroyed this method will not forcely find the user
		end
	end
	def my_tweets
		if Current.reg.nil?
			redirect_to signin_path,notice:'You must be login'
		else
			@userTweets=Current.reg.tweets
		end
	end

end