#db has email:strign field and password_digest:string field

#but our has_secure_password adds two new attribute password and password_confirmation
# those are virtual attributes and used to check the password and password conformation matches or not

# then it will store the hashed password in db



class User < ApplicationRecord
	has_secure_password
	has_many :tweets

	validates :email, presence: true, format: {with: /^(|(([A-Za-z0-9]+_+)|([A-Za-z0-9]+\-+)|([A-Za-z0-9]+\.+)|([A-Za-z0-9]+\++))*[A-Za-z0-9]+@((\w+\-+)|(\w+\.))*\w{1,63}\.[a-zA-Z]{2,6})$/i , message: "Must be a valid email",:multiline => true} # multiline true is used to confirm that multilne accept

end
