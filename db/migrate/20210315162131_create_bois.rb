class CreateBois < ActiveRecord::Migration[6.1]
  def change
    create_table :bois do |t|
      t.string :name
      t.string :author
      t.string :subj
      t.integer :price

      t.timestamps
    end
  end
end
