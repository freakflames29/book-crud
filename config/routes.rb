Rails.application.routes.draw do

  resources :tweets
	#routing our own resources
	# get 'foods' ,to: "foods#index" ,:as=>"foods"
	# get 'foods/new', to:"foods#new", :as=>"new_foods"
	# post 'foods' ,to:'foods#create'
	# get 'foods/:id/edit', to:"foods#edit" ,:as=>"edit_foods"

	# get 'foods/:id', to:"foods#show",:as=>"food"
	# patch 'foods/:id', to:"foods#update"
	# put 'foods/:id', to:"foods#update"
	# delete 'foods/:id', to:'foods#destroy'

  # resources :bois
  # resources :books
	# root 'foods#index'
	# root 'mains#index'
  root 'tweets#index'


	get'signup', to:'registrations#new'
  	post 'signup', to:'registrations#create' #creating a post url for create method 

  	#logout 
  	delete 'logout', to:'sessions#destroy'


  	#login method
  	get 'signin', to:'sessions#new' # this for getting the form
  	post 'signin', to:'sessions#create' # this is for posting the form

  	#edit password
  	get 'password/edit', to:'passwordedits#edit'
  	patch 'password/edit', to:'passwordedits#update'

      # user tweets
      get 'mytweets',to:'mains#my_tweets'
  #added a comment
  # For details on the DSL sid_available?able within this file, see https://guides.rubyonrails.org/routing.html
end
