require "application_system_test_case"

class BoisTest < ApplicationSystemTestCase
  setup do
    @boi = bois(:one)
  end

  test "visiting the index" do
    visit bois_url
    assert_selector "h1", text: "Bois"
  end

  test "creating a Boi" do
    visit bois_url
    click_on "New Boi"

    fill_in "Author", with: @boi.author
    fill_in "Name", with: @boi.name
    fill_in "Price", with: @boi.price
    fill_in "Subj", with: @boi.subj
    click_on "Create Boi"

    assert_text "Boi was successfully created"
    click_on "Back"
  end

  test "updating a Boi" do
    visit bois_url
    click_on "Edit", match: :first

    fill_in "Author", with: @boi.author
    fill_in "Name", with: @boi.name
    fill_in "Price", with: @boi.price
    fill_in "Subj", with: @boi.subj
    click_on "Update Boi"

    assert_text "Boi was successfully updated"
    click_on "Back"
  end

  test "destroying a Boi" do
    visit bois_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Boi was successfully destroyed"
  end
end
