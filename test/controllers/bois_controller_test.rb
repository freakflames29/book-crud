require "test_helper"

class BoisControllerTest < ActionDispatch::IntegrationTest
  setup do
    @boi = bois(:one)
  end

  test "should get index" do
    get bois_url
    assert_response :success
  end

  test "should get new" do
    get new_boi_url
    assert_response :success
  end

  test "should create boi" do
    assert_difference('Boi.count') do
      post bois_url, params: { boi: { author: @boi.author, name: @boi.name, price: @boi.price, subj: @boi.subj } }
    end

    assert_redirected_to boi_url(Boi.last)
  end

  test "should show boi" do
    get boi_url(@boi)
    assert_response :success
  end

  test "should get edit" do
    get edit_boi_url(@boi)
    assert_response :success
  end

  test "should update boi" do
    patch boi_url(@boi), params: { boi: { author: @boi.author, name: @boi.name, price: @boi.price, subj: @boi.subj } }
    assert_redirected_to boi_url(@boi)
  end

  test "should destroy boi" do
    assert_difference('Boi.count', -1) do
      delete boi_url(@boi)
    end

    assert_redirected_to bois_url
  end
end
